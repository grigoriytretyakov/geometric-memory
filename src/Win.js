import { WIN, MENU, WIN_SPRITE, settings } from './settings';

class Win extends Phaser.Scene {
    constructor() {
        super({key: WIN});
    }

    create() {
        document.body.style.backgroundColor = '#' + settings.backgroundColor.toString(16);

        this.add.image(settings.gameWidth / 2, settings.gameHeight / 2, WIN_SPRITE);

        this.input.on("pointerup", this.handleTouch, this);
    }

    handleTouch(e) {
        this.scene.start(MENU);
    }
}

export { Win }
