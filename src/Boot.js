import {
    BOOT,
    MENU,
    CHOOSE_SPRITE,
    WIN_SPRITE,
    SOUNDBUTTON_SPRITE,
    FLIP,
    MATCH
} from './settings';

class Boot extends Phaser.Scene {
    constructor() {
        super({key: BOOT})
    }

    preload() {
        this.load.image(CHOOSE_SPRITE, 'assets/choosepalette.png');
        this.load.image(WIN_SPRITE, 'assets/win.png');
        this.load.spritesheet(SOUNDBUTTON_SPRITE, 'assets/soundbutton.png', 
            {frameWidth: 150, frameHeight: 150});

        this.load.audio(FLIP, 'assets/click.wav');
        this.load.audio(MATCH, 'assets/match.wav');
    }

    create() {
        this.scene.start(MENU);
    }

}

export { Boot };
