import { MENU, GAME, CHOOSE_SPRITE, SOUNDBUTTON_SPRITE, FLIP, settings } from './settings';


class Menu extends Phaser.Scene {
    constructor() {
        super({key: MENU});
    }

    create() {
        document.body.style.backgroundColor = '#' + settings.backgroundColor.toString(16);

        let panel = this.add.graphics();
        panel.fillStyle(settings.backgroundColor, 1)
            .fillRect(0, 0, settings.gameWidth, settings.gameHeight);

        const padding = 80;

        this.add.image(settings.gameWidth / 2, padding, CHOOSE_SPRITE).setOrigin(0.5, 0);

        this.flipSound = this.sound.add(FLIP);
        this.soundEnabeld = true;
        let soundBtn = this.add.image(
            settings.gameWidth / 2,
            settings.gameHeight - padding,
            SOUNDBUTTON_SPRITE);
        soundBtn.setOrigin(0.5, 1).setInteractive().on('pointerup', () => {
            this.soundEnabeld = !this.soundEnabeld;
            if (this.soundEnabeld) {
                soundBtn.setFrame(0);
                this.flipSound.play();
            }
            else {
                soundBtn.setFrame(1);
            }
        }, this);

        this.palettesIntervals = [];
        const startY = 230;
        for (let paletteNumber = 0; paletteNumber < settings.palette.length; paletteNumber++) {
            let y = startY + (settings.cardSize + settings.paletteOffset) * paletteNumber 
            this.palettesIntervals.push([y, y + settings.cardSize, paletteNumber]);
            for (let i = 0; i < settings.boardSize.cols; i++) {
                let x = (settings.cardSize + settings.cardOffset * 2) * i + settings.cardOffset;
                panel.fillStyle(settings.palette[paletteNumber][i], 1)
                    .fillRoundedRect(
                        x, y, 
                        settings.cardSize,
                        settings.cardSize,
                        settings.cardRoundRadius);
            }
        }

        this.input.on("pointerup", this.handleTouch, this);
    }

    handleTouch(e) {
        let y = e.upY;

        for (let [y1, y2, paletteNumber] of this.palettesIntervals) {
            if (y > y1 && y < y2) {
                let palette = settings.palette[paletteNumber];
                this.scene.start(
                    GAME,
                    {
                        palette: palette,
                        soundEnabeld: this.soundEnabeld
                    }
                );
                return;
            }
        }
    }
}

export { Menu }
