export const BOOT = 'Boot';
export const MENU = 'Menu';
export const GAME = 'Game';
export const WIN = 'Win';

export const CHOOSE_SPRITE = 'choose';
export const WIN_SPRITE = 'win';
export const SOUNDBUTTON_SPRITE = 'soundbutton';

export const FLIP = 'flip';
export const MATCH = 'match';

const gameWidth = 800;

const boardSize = {
    cols: 4,
    rows: 7,
};

const gameHeight = gameWidth * boardSize.rows / boardSize.cols;

const cardOffset = 5;

const cardSpace = gameWidth / boardSize.cols;

const cardSize = cardSpace - cardOffset * 2;

const cardPadding = cardSize * 0.1;

const squarePadding = (cardSize - cardPadding * 2) / 6;

const circlePadding = squarePadding;

const triangleLength = (cardSize - cardPadding * 2) * 2 * Math.sqrt(1 / 3);

const trianglePadding = triangleLength / 5;

const palette = [
    [
        0x352e4d,
        0x562e73,
        0x8f2e99,
        0xb32d91,
    ],
    [
        0x2e4d2e,
        0x2e7345,
        0x2e9975,
        0x2db3b3,
    ],
    [
        0xf2bcaa,
        0xe57e88,
        0xcc5285,
        0xb32d91,
    ],
];

const settings = {
    gameWidth,
    gameHeight,

    palette,

    paletteOffset: 100,

    backgroundColor: 0x222222,

    boardSize,

    cardSpace,
    cardSize,
    cardPadding,
    cardOffset,
    cardRoundRadius: 5,
    cardBorderAlpha: 1,
    cardBorderWidth: 2,

    fillAlpha: 1,

    squarePadding,
    circleRadius: cardSize / 2,
    circlePadding,
    trianglePadding,
    triangleLength,

    turnOverDuration: 50,
    matchDuration: 600,
};


export { settings };
