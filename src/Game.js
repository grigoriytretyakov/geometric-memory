import { GAME, WIN, FLIP, MATCH, settings } from './settings';


const CIRCLE = 0, SQUARE = 1, TRIANGLE = 2;


class Game extends Phaser.Scene {
    constructor() {
        super({key: GAME});
    }

    init(props) {
        this.soundEnabeld = props.soundEnabeld;
        this.palette = props.palette;

        [this.COLOR_0, this.COLOR_1, this.COLOR_2, this.COLOR_3] = this.palette;

        this.backgroundColor = this.COLOR_0;

        this.cardBorderColor = this.COLOR_3;
    }

    create() {
        document.body.style.backgroundColor = '#' + this.backgroundColor.toString(16);

        this.items = null;
        this.openedOne = null;
        this.openedTwo = null;
        this.canPlay = true;
        this.items = [];

        this.flipSound = null;
        this.matchSound = null;
        if (this.soundEnabeld) {
            this.flipSound = this.sound.add(FLIP);
            this.matchSound = this.sound.add(MATCH);
        }

        this.generateGame();

        this.input.on("pointerup", this.handleTouch, this);
    }

    generateGame() {
        this.add.graphics()
            .fillStyle(this.backgroundColor, 1)
            .fillRect(0, 0, settings.gameWidth, settings.gameHeight);

        const types = this.createTypes();
        this.createGameCards(types);
    }

    createTypes() {
        const permutations = [
            [0, 1, 2],
            [0, 2, 1],
            [1, 0, 2],
            [1, 2, 0],
            [2, 1, 0],
            [2, 0, 1],
        ];

        const colors = [
            this.COLOR_1,
            this.COLOR_2,
            this.COLOR_3
        ];

        let types = [];
        for (let i = 0; i < 6; i++) {
            [CIRCLE, SQUARE, TRIANGLE].forEach(shape => {
                types.push({
                    shape: shape,
                    colors: [
                        colors[permutations[i][0]],
                        colors[permutations[i][1]],
                        colors[permutations[i][2]],
                    ],
                    name: `${shape}${permutations[i][0]}${permutations[i][1]}${permutations[i][2]}`,
                });
            })
        }

        types = Phaser.Utils.Array.Shuffle(types);
        types = types.slice(0, settings.boardSize.cols * settings.boardSize.rows / 2);
        types = Phaser.Utils.Array.Shuffle(types.concat(types));

        return types;
    }

    createGameCards(types) {
        for (let i = 0; i < settings.boardSize.rows; i++) {
            this.items.push([]);
            let y = (settings.cardSize + settings.cardOffset * 2) * i + settings.cardOffset;
            for (let j = 0; j < settings.boardSize.cols; j++) {
                let x = (settings.cardSize + settings.cardOffset * 2) * j + settings.cardOffset;

                const type = types.shift();
                const { card, border } = this.createCard(x, y, type);

                this.items[i].push({
                    i,
                    j,
                    card,
                    border,
                    name: type.name,
                })
            }
        }
    }

    handleTouch(e) {
        if (this.canPlay) {
            this.canPlay = false;

            let col = Math.floor(e.upY / settings.cardSpace);
            let row = Math.floor(e.upX / settings.cardSpace);

            if (!this.items[col][row]) {
                this.canPlay = true;
                return;
            }

            if (this.openedOne && this.openedTwo) {
                this.tweens.add({
                    targets: [this.openedOne.card, this.openedTwo.card],
                    duration: settings.turnOverDuration,
                    alpha: 0,
                    onComplete: () => {
                        this.openedTwo = null;
                        this.openedOne = this.items[col][row];

                        this.playSound(this.flipSound);
                        this.tweens.add({
                            targets: [this.openedOne.card],
                            duration: settings.turnOverDuration,
                            alpha: 1,
                            onComplete: () => {
                                this.canPlay = true;
                            }
                        });
                    }
                });
            }
            else if (this.openedOne) {
                if (this.openedOne.i !== col || this.openedOne.j !== row) {
                    this.openedTwo = this.items[col][row];

                    this.tweens.add({
                        targets: [this.openedTwo.card],
                        duration: settings.turnOverDuration,
                        alpha: 1,
                        onComplete: () => {
                            if (this.openedOne.name === this.openedTwo.name) {
                                this.playSound(this.matchSound);
                                this.tweens.add({
                                    targets: [
                                        this.openedOne.card,
                                        this.openedOne.border,
                                        this.openedTwo.card,
                                        this.openedTwo.border,
                                    ],
                                    duration: settings.matchDuration,
                                    y: settings.gameHeight + settings.cardSize + 50,
                                    onComplete: () => {
                                        this.openedOne.border.destroy();
                                        this.openedOne.card.destroy();
                                        this.openedTwo.border.destroy();
                                        this.openedTwo.card.destroy();

                                        this.items[this.openedOne.i][this.openedOne.j] = null;
                                        this.items[this.openedTwo.i][this.openedTwo.j] = null;

                                        this.openedTwo = null;
                                        this.openedOne = null;

                                        this.canPlay = true;

                                        for (let line of this.items) {
                                            for (let item of line) {
                                                if (item) {
                                                    return;
                                                }
                                            }
                                        }

                                        this.scene.start(WIN);
                                    }
                                });
                            }
                            else {
                                this.playSound(this.flipSound);
                                this.canPlay = true;
                            }
                        }
                    });
                }
                else {
                    this.canPlay = true;
                }
            }
            else {
                this.playSound(this.flipSound);
                this.openedOne = this.items[col][row];

                this.tweens.add({
                    targets: [this.openedOne.card],
                    duration: settings.turnOverDuration,
                    alpha: 1,
                    onComplete: () => {
                        this.canPlay = true;
                    }
                });
            }
        }
    }

    playSound(sound) {
        if (this.soundEnabeld) {
            sound.play();
        }
    }

    createCard(x, y, type) {
        let card = this.add.graphics();
        card.alpha = 0;

        switch(type.shape) {
            case CIRCLE:
                this.createCircle(x, y, card, type);
                break;
            case SQUARE:
                this.createSquare(x, y, card, type);
                break;
            case TRIANGLE:
                this.createTriangle(x, y, card, type);
                break;
        }

        let border = this.add.graphics();
        border.lineStyle(
            settings.cardBorderWidth,
            this.cardBorderColor,
            settings.cardBorderAlpha
        ).strokeRoundedRect(
            x, y,
            settings.cardSize,
            settings.cardSize,
            settings.cardRoundRadius);

        return {
            card,
            border
        };
    }

    createSquare(x, y, card, type) {
        for (let i = 0; i < 3; i++) {
            const padding = settings.squarePadding * i;

            card.fillStyle(type.colors[i], settings.fillAlpha)
                .fillRect(
                    x + settings.cardPadding + padding,
                    y + settings.cardPadding + padding,
                    settings.cardSize - 2 * (settings.cardPadding + padding),
                    settings.cardSize - 2 * (settings.cardPadding + padding));
        }
    }

    createTriangle(x, y, card, type) {
        for (let i = 0; i < 3; i++) {
            const padding = settings.trianglePadding * i;

            let scale = (settings.cardSize - settings.cardPadding * 2 - padding * 1.5) / (settings.cardSize - settings.cardPadding * 2);

            const length = settings.triangleLength * scale;

            let triangle = Phaser.Geom.Triangle.BuildEquilateral(
                x + settings.cardSize / 2,
                y + settings.cardPadding + padding,
                length
            );

            card.fillStyle(type.colors[i], settings.fillAlpha)
                .fillTriangleShape(triangle);
        }
    }

    createCircle(x, y, card, type) {
        for (let i = 0; i < 3; i++) {
            const padding = settings.circlePadding * i;

            card.fillStyle(type.colors[i], settings.fillAlpha)
                .fillCircle(
                    x + settings.cardSize / 2,
                    y + settings.cardSize / 2,
                    settings.circleRadius - (settings.cardPadding + padding));

        }
    }
}

export { Game }
